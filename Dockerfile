FROM busybox:latest

RUN mkdir build &&\
    cd build &&\
    touch house.txt &&\
    echo "walls" >> house.txt \
    echo "floor" >> house.txt \
    echo "Hi from running docker container" >> check.txt \
